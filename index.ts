import * as LimitEventsHandler from './src/handler/UserLimitEventsHandler';
import events from './input/events.json';
import {KinesisEvent} from './src/model/kinesis-events';
LimitEventsHandler.handleUserLimitEvents(events as KinesisEvent[]);
