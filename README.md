
## To install the required dependencies:

    npm i

## To run:

    npx tsc; node .\.build\index.js

## Development notes

Some notes on corners cut:

1. The kinesis event aws type was not used for simplicity and kinesis events were not validated as we rely omn aws for that
2. Some simple test cases were added.
3. Some logging was added as serverless aplication are a bit hard to debug so remote monitoring is usually nessessary.

## Answers to questions:

1. It was not too lengthy and the video was very interesting.

2. Since the environment is aws and the main operations would be a)get a UserLimit and b)update a UserLimit I would suggest dynamoDb which is a serverless high availability key value no sql database. (the use case is key value here). The additional tasks would be:

   a) define the operations that will happen on the data so we can create the appropriate indexes,

   b) define billing mode e.g. pay per request vs provisioned . Obvisouly here the provisioned mode might not be a great fit :P

   c) analyze cost of storing these data and define policies to move them to colder storage (colder = more rarely used).

   d) define Iam policies to access the respective tables, although this could happen at a later time when consumers are more thoroughly defined.

   e) define how to avoid synchronization problems, if any, on the specific use case

   f) define whether back up or point in time recovery might be nessesary

   g) define timetolive meaning when will the entries expire in the database if that is needed,

3. Based on the operations asked, I would opt for a simple rest api with the following format:

   //1. Get all userLimits

         GET users/{userId}/userLimits

   //2. Get a specific userLimit

        GET users/{userId}/userLimits/{userLimitId}

   //3. Create a userLimit

         POST users/{userId}/userLimits

4. Service, repository, and utils layers are reusable. I am not entirely sure which other functionality can be reused and how.
