import {
  EventsType,
  KinesisEvent,
  UserLimitCreatedEvent,
  UserLimitProgressChangedEvent,
  UserLimitResetEvent,
  USER_LIMIT_CREATED_EVENT_SCHEMA,
  USER_LIMIT_PROGRESS_CHANGED_SCHEMA,
  USER_LIMIT_RESET_SCHEMA,
} from '../model/kinesis-events';
import * as LimitEventsService from '../service/UserLimitEventsService';
import * as Validator from '../utils/Validator';

export function handleUserLimitEvents(events: KinesisEvent[]) {
  try {
    for (const event of events) {
      console.log('Handling event:' + event);
      try {
        switch (event.type) {
          case EventsType.USER_LIMIT_CREATED: {
            Validator.validateSchema(
              event.payload,
              USER_LIMIT_CREATED_EVENT_SCHEMA
            );
            LimitEventsService.handleUserLimitCreated(
              event.payload as UserLimitCreatedEvent
            );
            break;
          }
          case EventsType.USER_LIMIT_RESET: {
            Validator.validateSchema(event.payload, USER_LIMIT_RESET_SCHEMA);
            LimitEventsService.handleUserLimitReset(
              event.payload as UserLimitResetEvent
            );
            break;
          }
          case EventsType.USER_LIMIT_PROGRESS_CHANGED: {
            Validator.validateSchema(
              event.payload,
              USER_LIMIT_PROGRESS_CHANGED_SCHEMA
            );
            LimitEventsService.handleUserLimitProgressChanged(
              event.payload as UserLimitProgressChangedEvent
            );
            break;
          }
          default: {
            throw new Error(`Event with type ${event.type} is not handled yet`);
          }
        }
      } catch (err) {
        console.log(err);
        DeadLetterQueue.publishError(err);
      }
    }
  } catch (err) {
    console.log(err);
  }
}
