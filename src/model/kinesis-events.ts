import {LimitPeriod, LimitStatus, LimitType} from './user-limit';
import * as Joi from 'joi';
export enum EventsType {
  USER_LIMIT_PROGRESS_CHANGED = 'USER_LIMIT_PROGRESS_CHANGED',
  USER_LIMIT_CREATED = 'USER_LIMIT_CREATED',
  USER_LIMIT_RESET = 'USER_LIMIT_RESET',
}

export interface KinesisEvent {
  aggregateId: string;
  context: {
    correlationId?: string;
  };
  createdAt: number;
  eventId?: string;
  payload:
    | UserLimitCreatedEvent
    | UserLimitProgressChangedEvent
    | UserLimitResetEvent;
  sequenceNumber: number;
  source: string;
  type: EventsType;
}

export interface UserLimitCreatedEvent {
  activeFrom: number;
  brandId: string;
  currencyCode: string;
  nextResetTime: number;
  period: LimitPeriod;
  status: LimitStatus;
  type: LimitType;
  userId: string;
  userLimitId: string;
  value: string;
}

export const USER_LIMIT_CREATED_EVENT_SCHEMA = Joi.object({
  activeFrom: Joi.number().required(),
  brandId: Joi.string().required(),
  currencyCode: Joi.string().required(),
  nextResetTime: Joi.number().required(),
  period: Joi.string().valid(...Object.values(LimitPeriod)),
  status: Joi.string().valid(...Object.values(LimitStatus)),
  type: Joi.string().valid(...Object.values(LimitType)),
  userId: Joi.string().required(),
  userLimitId: Joi.string().required(),
  value: Joi.string().required(),
});

export interface UserLimitProgressChangedEvent {
  amount: string;
  brandId: string;
  currencyCode: string;
  nextResetTime: number;
  previousProgress: string;
  remainingAmount: string;
  userId: string;
  userLimitId: string;
}

export const USER_LIMIT_PROGRESS_CHANGED_SCHEMA = Joi.object({
  amount: Joi.string().required(),
  brandId: Joi.string().required(),
  currencyCode: Joi.string().required(),
  nextResetTime: Joi.number().required(),
  previousProgress: Joi.string().required(),
  remainingAmount: Joi.string().required(),
  userId: Joi.string().required(),
  userLimitId: Joi.string().required(),
});

export interface UserLimitResetEvent {
  brandId: string;
  currencyCode: string;
  nextResetTime: number;
  period: string;
  resetAmount: string;
  resetPercentage: string;
  type: string;
  unusedAmount: string;
  userId: string;
  userLimitId: string;
}

export const USER_LIMIT_RESET_SCHEMA = Joi.object({
  brandId: Joi.string().required(),
  currencyCode: Joi.string().required(),
  nextResetTime: Joi.number().required(),
  period: Joi.string().required(),
  resetAmount: Joi.string().required(),
  resetPercentage: Joi.string().required(),
  type: Joi.string().required(),
  unusedAmount: Joi.string().required(),
  userId: Joi.string().required(),
  userLimitId: Joi.string().required(),
});
