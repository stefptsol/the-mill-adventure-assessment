import {
  UserLimitCreatedEvent,
  UserLimitProgressChangedEvent,
} from '../model/kinesis-events';
import {UserLimit} from '../model/user-limit';

export function toUserLimit(event: UserLimitCreatedEvent): UserLimit {
  const userLimit: UserLimit = {
    activeFrom: event.activeFrom,
    brandId: event.brandId,
    createdAt: event.activeFrom, //I am setting this value here for simplicity
    currencyCode: event.currencyCode,
    nextResetTime: event.nextResetTime,
    period: event.period,
    status: event.status,
    type: event.type,
    userId: event.userId,
    userLimitId: event.userLimitId,
    value: event.value,
  };
  return userLimit;
}

export function toUpdatedUserLimit(
  userLimit: UserLimit,
  userLimitProgressChangedEvent: UserLimitProgressChangedEvent
): UserLimit {
  let progress = 0;
  if (userLimit.progress) {
    progress =
      parseFloat(userLimit.progress) +
      parseFloat(userLimitProgressChangedEvent.amount);
  } else {
    progress = +userLimitProgressChangedEvent.amount;
  }
  const updaredUserLimit = {
    ...userLimit,
    progress: '' + progress,
  };
  return updaredUserLimit;
}

export function toResetedUserLimit(userLimit: UserLimit): UserLimit {
  const updaredUserLimit = {
    ...userLimit,
    progress: '0',
  };
  return updaredUserLimit;
}
