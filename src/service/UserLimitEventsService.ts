import {
  UserLimitCreatedEvent,
  UserLimitProgressChangedEvent,
  UserLimitResetEvent,
} from '../model/kinesis-events';
import * as UserLimitEventsConverter from '../converter/UserLimitEventsConverter';
import {UserLimit} from '../model/user-limit';
import * as UserLimitEventsRepository from '../repository/UserLimitEventsRepository';

/**
 * Stores a UserLimit entity on the database from a UserLimitCreated event
 * @param event
 */
export function handleUserLimitCreated(
  userLimitCreatedEvent: UserLimitCreatedEvent
): void {
  const userLimit: UserLimit = UserLimitEventsConverter.toUserLimit(
    userLimitCreatedEvent
  );
  UserLimitEventsRepository.saveUserLimit(userLimit);
}

/**
 * Updates the progress of a UserLimit based on a UserLimitProgressChangedEvent
 * Throws an error if UserLimit described on UserLimitProgressChangedEvent is not found
 * @param userLimitProgressChangedEvent
 */
export function handleUserLimitProgressChanged(
  userLimitProgressChangedEvent: UserLimitProgressChangedEvent
) {
  const userLimit = UserLimitEventsRepository.findUserLimit(
    userLimitProgressChangedEvent.userLimitId
  );
  if (!userLimit) {
    throw new Error(
      `User limit ${userLimitProgressChangedEvent.userLimitId} not found`
    );
  }
  const updatedUserLimit = UserLimitEventsConverter.toUpdatedUserLimit(
    userLimit,
    userLimitProgressChangedEvent
  );
  UserLimitEventsRepository.saveUserLimit(updatedUserLimit);
}

/**
 * Resets the progress of a UserLimit based on a UserLimitResetEvent
 * Throws an error if UserLimit described on UserLimitResetEvent is not found
 * @param userLimitResetEvent
 */
export function handleUserLimitReset(userLimitResetEvent: UserLimitResetEvent) {
  const userLimit = UserLimitEventsRepository.findUserLimit(
    userLimitResetEvent.userLimitId
  );
  if (!userLimit) {
    throw new Error(`User limit ${userLimitResetEvent.userLimitId} not found`);
  }
  const updatedUserLimit =
    UserLimitEventsConverter.toResetedUserLimit(userLimit);
  UserLimitEventsRepository.saveUserLimit(updatedUserLimit);
}
