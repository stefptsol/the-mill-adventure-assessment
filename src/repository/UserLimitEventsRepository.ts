import {UserLimit} from '../model/user-limit';
import * as Database from '../utils/Database';

export function saveUserLimit(userLimit: UserLimit): void {
  Database.userLimitEventsTable.set(userLimit.userLimitId, userLimit);
}

export function findUserLimit(userLimitId: string): UserLimit {
  return Database.userLimitEventsTable.get(userLimitId);
}
