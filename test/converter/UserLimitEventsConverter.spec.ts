import 'mocha';
import * as UserLimitEventsConverter from '../../src/converter/UserLimitEventsConverter';
import {
  UserLimitCreatedEvent,
  UserLimitProgressChangedEvent,
} from '../../src/model/kinesis-events';
import {
  LimitPeriod,
  LimitStatus,
  LimitType,
  UserLimit,
} from '../../src/model/user-limit';
import {expect} from 'chai';

describe('UserLimitEvent converter tests', () => {
  it('Should create a userLimit', () => {
    const userLimitCreatedEvent: UserLimitCreatedEvent = {
      activeFrom: 1647946090592,
      brandId: '000000000000000000000001',
      currencyCode: 'SEK',
      nextResetTime: 1648032490592,
      period: LimitPeriod.DAY,
      status: LimitStatus.ACTIVE,
      type: LimitType.DEPOSIT,
      userId: 'VijPYTEOgK7dxLs5fBjJ',
      userLimitId: 'LKMgxoE0yFgH6F6iShEu',
      value: '10000',
    };

    const result: UserLimit = UserLimitEventsConverter.toUserLimit(
      userLimitCreatedEvent
    );

    expect(result.progress).to.be.undefined;
    expect(result.value).to.equal(userLimitCreatedEvent.value);
  });

  it('Should update a userLimit', () => {
    const userLimitCreatedEvent: UserLimitProgressChangedEvent = {
      brandId: '000000000000000000000001',
      currencyCode: 'SEK',
      nextResetTime: 1648032490592,
      userId: 'VijPYTEOgK7dxLs5fBjJ',
      userLimitId: 'LKMgxoE0yFgH6F6iShEu',
      amount: '300',
      previousProgress: '0',
      remainingAmount: '22322',
    };

    const userLimit: UserLimit = {
      activeFrom: 1647946090592,
      brandId: '000000000000000000000001',
      currencyCode: 'SEK',
      nextResetTime: 1648032490592,
      period: LimitPeriod.DAY,
      status: LimitStatus.ACTIVE,
      type: LimitType.DEPOSIT,
      userId: 'VijPYTEOgK7dxLs5fBjJ',
      userLimitId: 'LKMgxoE0yFgH6F6iShEu',
      value: '10000',
    };
    const result: UserLimit = UserLimitEventsConverter.toUpdatedUserLimit(
      userLimit,
      userLimitCreatedEvent
    );

    expect(result.progress).to.not.be.undefined;
    expect(result.progress).to.equal('300');
  });
});
