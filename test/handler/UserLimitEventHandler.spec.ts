import 'mocha';
import * as UserLimitEventsHandler from '../../src/handler/UserLimitEventsHandler';
import {KinesisEvent} from '../../src/model/kinesis-events';
import * as sinon from 'sinon';
import * as DeadLetterQueue from '../../src/utils/DeadLetterQueue';

describe('UserLimitEvent handler tests', () => {
  let DeadLetterQueueMock: any;
  beforeEach(() => {
    DeadLetterQueueMock = sinon.mock(DeadLetterQueue);
  });
  afterEach(() => {
    DeadLetterQueueMock.verify();
    DeadLetterQueueMock.restore();
  });

  it('Should throw a validation error when a UserLimitCreatedEvent does not have a progress attribute ', () => {
    const kinesisEvents = [
      {
        aggregateId: 'VijPYTEOgK7dxLs5fBjJ',
        context: {
          correlationId: 'hVyFHScCNAmSyAPulhtsQ',
        },
        createdAt: 1647946090594,
        eventId: 'HENgJu3fBmWWtVjlifo4',
        payload: {
          activeFrom: 1647946090592,
          brandId: '000000000000000000000001',
          currencyCode: 'SEK',
          nextResetTime: 1648032490592,
          period: 'DAY',
          status: 'ACTIVE',
          type: 'DEPOSIT',
          userId: 'VijPYTEOgK7dxLs5fBjJ',
          userLimitId: 'LKMgxoE0yFgH6F6iShEu',
          value: '10000',
        },
        sequenceNumber: 3,
        source: 'limitUser',
        type: 'USER_LIMIT_CREATED',
      },
      {
        aggregateId: 'VijPYTEOgK7dxLs5fBjJ',
        context: {
          correlationId: 'hVyFHScCNAmSyAPulhtsQ',
        },
        createdAt: 1647946101223,
        eventId: 'G5xplaPbCdVQ4CmZZYgg',
        payload: {
          brandId: '000000000000000000000001',
          currencyCode: 'SEK',
          nextResetTime: 1648032490592,
          previousProgress: '0',
          remainingAmount: '9800.00',
          userId: 'VijPYTEOgK7dxLs5fBjJ',
          userLimitId: 'LKMgxoE0yFgH6F6iShEu',
        },
        sequenceNumber: 6,
        source: 'limitUser',
        type: 'USER_LIMIT_PROGRESS_CHANGED',
      },
    ];

    DeadLetterQueueMock.expects('publishError').once(); //I am cutting a corner here and not defining the exact error returned

    UserLimitEventsHandler.handleUserLimitEvents(
      kinesisEvents as KinesisEvent[]
    );
  });
});
